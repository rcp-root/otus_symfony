<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 * @ORM\Table(name="courses")
 * @ORM\HasLifecycleCallbacks()
 */
class Course extends BaseEntity
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=32)
     */
    private string $name;

    /**
     * @var Collection
     * @ORM\OneToMany (targetEntity="Group", mappedBy="courses", cascade={"persist"})
     */
    private Collection $groups;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="course", cascade={"persist"}, orphanRemoval=true)
     */
    private Collection $lessons;

    /**
     * Course constructor.
     */
    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->lessons = new ArrayCollection();
    }

    /**
     * @param Group $groups
     */
    public function addGroup(Group $groups): void
    {
        if (!$this->groups->contains($groups)) {
            $this->groups->add($groups);
        }
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @param Lesson $lessons
     */
    public function addLesson(Lesson $lessons): void
    {
        if (!$this->lessons->contains($lessons)) {
            $this->lessons->add($lessons);
        }
    }

    /**
     * @return Collection
     */
    public function getLesson(): Collection
    {
        return $this->lessons;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'lesson' => $this->getLesson()->toArray(),
            'groups' => $this->getGroups()->toArray()
        ];
    }
}
