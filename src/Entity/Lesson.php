<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LessonRepository")
 * @ORM\Table(
 *     name="lessons",
 *     indexes={
 *      @ORM\Index(name="index_revision_id", columns={"revision_id"}),
 *      @ORM\Index(name="index_course_id", columns={"course_id"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Lesson extends BaseEntity
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=32)
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=300)
     */
    private string $description;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Task", inversedBy="lessons")
     * @ORM\JoinTable(
     *     name="tasks_2_lessons",
     *     joinColumns={@ORM\JoinColumn(name="lesson_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")}
     * )
     */
    private Collection $tasks;

    /**
     * @var Revision
     * @ORM\ManyToOne(targetEntity="Revision")
     * @ORM\JoinColumns{(
     * @ORM\JoinColumn(name="revision_id", referencedColumnName="id")
     * )}
     */
    private Revision $revision;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="lessons", cascade={"persist"})
     * @ORM\JoinColumns{(
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", onDelete="CASCADE")
     * )}
     */
    private Course $course;

    /**
     * Lesson constructor.
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param Task $tasks
     */
    public function addTask(Task $tasks): void
    {
        if (!$this->tasks->contains($tasks)) {
            $this->tasks->add($tasks);
        }
    }

    /**
     * @return Revision
     */
    public function getRevision(): Revision
    {
        return $this->revision;
    }

    /**
     * @param Revision $revision
     */
    public function setRevision(Revision $revision): void
    {
        $this->revision = $revision;
    }

    /**
     * @return Course
     */
    public function getCourse():Course
    {
        return $this->course;
    }

    /**
     * @param Course $course
     */
    public function setCourse(Course $course): void
    {
        $this->course = $course;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'describe' => $this->getDescription(),
            'course' => $this->getCourse()->toArray(),
            'task' => $this->getTasks()->toArray(),
            'revision' => $this->getRevision()->toArray()
        ];
    }
}
