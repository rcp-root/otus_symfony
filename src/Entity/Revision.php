<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RevisionRepository")
 * @ORM\Table(name="revisions")
 * @ORM\HasLifecycleCallbacks()
 */
class Revision extends BaseEntity
{
    public function toArray(): array
    {
        return [
            'id' => $this->getId()
        ];
    }
}
