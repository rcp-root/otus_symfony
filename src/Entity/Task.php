<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ORM\Table(name="tasks")
 * @ORM\HasLifecycleCallbacks()
 */
class Task extends BaseEntity
{
    /**
     * @var string
     * @ORM\Column(name="name", nullable=false, type="string", length=32)
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column (name="description", type="text", nullable=false)
     */
    private string $description;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Lesson", mappedBy="tasks")
     */
    private Collection $lessons;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->lessons = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * @param Lesson $lessons
     */
    public function addLessons(Lesson $lessons): void
    {
        if (!$this->lessons->contains($lessons)) {
            $this->lessons->add($lessons);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'lesson' => $this->lessons->toArray(),
        ];
    }

}
