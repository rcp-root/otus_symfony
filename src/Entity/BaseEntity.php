<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class BaseEntity
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\Column(name="id", unique=true, type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected ?int $id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @var DateTimeImmutable
     * @ORM\Column (name="date_create", nullable=false, type="date_immutable")
     */
    protected DateTimeImmutable $dateCreate;

    /**
     * @var DateTimeImmutable
     * @ORM\Column (name="date_update", nullable=false, type="date_immutable")
     */
    protected DateTimeImmutable $dateUpdate;

    /**
     * @return DateTimeImmutable
     */
    public function getDateCreate(): DateTimeImmutable
    {
        return $this->dateCreate;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setDateCreate(): void
    {
        $this->dateCreate = new DateTimeImmutable();
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateUpdate(): DateTimeImmutable
    {
        return $this->dateUpdate;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setDateUpdate(): void
    {
        $this->dateUpdate = new DateTimeImmutable();
    }

    abstract public function toArray();
}
