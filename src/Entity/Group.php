<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 * @ORM\Table(
 *     name="user_groups",
 *     indexes={
 *      @ORM\Index(name="index_second_course__id", columns={"course_id"}),
 *      @ORM\Index(name="index_second_revision_id", columns={"revision_id"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Group extends BaseEntity
{
    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="groups")
     * @ORM\JoinColumns{(
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", onDelete="CASCADE")
     * )}
     */
    private Course $courses;

    /**
     * @var Revision
     * @ORM\ManyToOne(targetEntity="Revision")
     * @ORM\JoinColumns{(
     * @ORM\JoinColumn(name="revision_id", referencedColumnName="id", onDelete="CASCADE")
     * )}
     */
    private Revision $revision;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     */
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return Course
     */
    public function getCourses(): Course
    {
        return $this->courses;
    }

    /**
     * @param Course $courses
     */
    public function setCourses(Course $courses): void
    {
        $this->courses = $courses;
    }

    /**
     * @return Revision
     */
    public function getRevision(): Revision
    {
        return $this->revision;
    }

    /**
     * @param Revision $revision
     */
    public function setRevision(Revision $revision): void
    {
        $this->revision = $revision;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param User $users
     */
    public function addUsers(User $users): void
    {
        if (!$this->users->contains($users)) {
            $this->users->add($users);
        }
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'courses' => $this->getCourses()->getId(),
            'users' => array_map(static fn(User $user) => $user->getId(), $this->users->toArray()),
            'revision' => $this->getRevision()->toArray()
        ];
    }
}
