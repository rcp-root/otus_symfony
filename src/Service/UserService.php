<?php

namespace App\Service;

use App\Entity\Course;
use App\Entity\Group;
use App\Entity\Revision;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Yaml\Yaml;

class UserService extends BaseService
{
    private ObjectRepository $userRepository;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->userRepository = $this->entityManager->getRepository(User::class);
    }

    /**
     * @param string $name
     * @param string $login
     * @param string $passHash
     * @param string $active
     * @return User
     */
    public function createUser(string $name, string $login, string $passHash, string $active): User
    {
        $newUser = new User();
        $newUser->setName('test');
        $newUser->setLogin('login');
        $newUser->setPasswordHash('543535');
        $newUser->setActive('Y');
        $this->entityManager->persist($newUser);
        $this->entityManager->flush();

        return $newUser;
    }
}
