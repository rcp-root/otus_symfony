<?php


namespace App\Service;


use App\Entity\Course;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class CourseService extends BaseService
{
    private ObjectRepository $courseRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->courseRepository = $this->entityManager->getRepository(Course::class);
    }

    public function createCourse(string $name): Course
    {
        $newCourse = new Course();
        $newCourse->setName($name);

        $this->entityManager->persist($newCourse);
        $this->entityManager->flush();

        return $newCourse;
    }
}