<?php


namespace App\Service;


use App\Entity\Course;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class TaskService extends BaseService
{
    private ObjectRepository $taskRepository;

    /**
     * TaskService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->taskRepository = $this->entityManager->getRepository(Task::class);
    }

    /**
     * @param string $name
     * @param string $description
     * @return Task
     */
    public function createTask(string $name, string $description): Task
    {
        $task = new Task();
        $task->setName($name);
        $task->setDescription($description);

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $task;
    }
}