<?php

namespace App\Service;

use App\Entity\Course;
use App\Entity\Lesson;
use App\Entity\Revision;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class LessonService extends BaseService
{
    private ObjectRepository $lessonRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->lessonRepository = $this->entityManager->getRepository(Lesson::class);
    }

    /**
     * @param string $name
     * @param string $description
     * @param int $courseId
     * @param array $taskId
     * @return Lesson
     */
    public function createLesson(string $name, string $description, int $courseId, array $taskId): Lesson
    {
        $this->taskRepository = $this->entityManager->getRepository(Task::class);
        $this->courseRepository = $this->entityManager->getRepository(Course::class);
        $this->revisionRepository = $this->entityManager->getRepository(Revision::class);

        $tasks = $this->taskRepository->findBy(['id' => [1,2,3]]);

        /** @var Course $course */
        $course = $this->courseRepository->findOneBy(['id' => $courseId]);

        /** @var Revision $revisions */
        $revisions = $this->revisionRepository->findBy([],['id' => 'desc'], 1);

        $lesson = new Lesson();

        foreach ($tasks as $task) {
            $lesson->addTask($task);
        }

        foreach ($revisions as $revision){
            $lesson->setRevision($revision);
        }

        $lesson->setCourse($course);
        $lesson->setName($name);
        $lesson->setDescription($description);

        $this->entityManager->persist($lesson);
        $this->entityManager->flush();

        return $lesson;
    }
}
