<?php


namespace App\Service;


use App\Entity\Course;
use App\Entity\Group;
use App\Entity\Revision;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class GroupService extends BaseService
{
    private ObjectRepository $groupRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->groupRepository = $this->entityManager->getRepository(Group::class);
    }

    /**
     * @param int $courseId
     * @param int $userId
     * @return Group
     */
    public function createGroup(int $courseId, int $userId): Group
    {
        $this->courseRepository = $this->entityManager->getRepository(Course::class);
        $this->userRepository = $this->entityManager->getRepository(User::class);
        $this->revisionRepository = $this->entityManager->getRepository(Revision::class);

        /** @var Course $course */
        $course = $this->courseRepository->findOneBy(['id' => $courseId]);
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['id' => $userId]);
        /** @var Revision $revision */
        $revisions = $this->revisionRepository->findBy([],['id' => 'desc'], 1);

        $newGroup = new Group();

        foreach ($revisions as $revision){
            $newGroup->setRevision($revision);
        }

        $newGroup->setCourses($course);
        $newGroup->addUsers($user);
        $course->addGroup($newGroup);
        $user->addGroup($newGroup);

        $this->entityManager->persist($newGroup);
        $this->entityManager->flush();

        return $newGroup;
    }

    /**
     * @param int $userId
     * @param int $groupId
     * @return Group
     */
    public function addUser2Group(int $userId, int $groupId): Group
    {
        $userRepository = $this->entityManager->getRepository(User::class);

        /** @var $user User */
        $user = $userRepository->findOneBy(['id' => $userId,]);
        /** @var $group Group */
        $group = $this->groupRepository->findOneBy(['id' => $groupId]);

        $user->addGroup($group);
        $group->addUsers($user);
        $this->entityManager->flush();

        return $group;
    }
}