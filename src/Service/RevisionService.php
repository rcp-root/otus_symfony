<?php

namespace App\Service;

use App\Entity\Course;
use App\Entity\Revision;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class RevisionService extends BaseService
{
    private ObjectRepository $revisionRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->revisionRepository = $this->entityManager->getRepository(Revision::class);
    }

    public function next(): Revision
    {
        $revision = new Revision();

        $this->entityManager->persist($revision);
        $this->entityManager->flush();

        return $revision;
    }
}
