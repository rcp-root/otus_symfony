<?php

namespace App\Controller;

use App\Service\CourseService;
use App\Service\GroupService;
use App\Service\LessonService;
use App\Service\RevisionService;
use App\Service\TaskService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TestController
{
    /**
     * @var UserService
     */
    private UserService $userService;

    /**
     * @var CourseService
     */
    private CourseService $courseService;

    /**
     * @var GroupService
     */
    private GroupService $groupService;

    /**
     * @var LessonService
     */
    private LessonService $lessonService;

    /**
     * @var RevisionService
     */
    private RevisionService $revisionService;

    /**
     * @var TaskService
     */
    private TaskService $taskService;

    public function __construct(
        UserService $userService,
        CourseService $courseService,
        GroupService $groupService,
        LessonService $lessonService,
        RevisionService $revisionService,
        TaskService $taskService
    ) {
        $this->userService = $userService;
        $this->courseService = $courseService;
        $this->groupService = $groupService;
        $this->lessonService = $lessonService;
        $this->revisionService = $revisionService;
        $this->taskService = $taskService;
    }

    /**
     * @Route ("/user", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createUserAction(Request $request): Response
    {
        $name = $request->request->get('name');
        $login = $request->request->get('login');
        $passHash = $request->request->get('pass_hash');
        $active = $request->request->get('active');

        $user = $this->userService->createUser($name, $login, $passHash, $active);

        return new JsonResponse($user->toArray(), 200);
    }

    /**
     * @Route ("/user/add_user_group", methods={"PUT"})
     * @param Request $request
     * @return Response
     */
    public function addUser2GroupAction(Request $request): Response
    {
        $userId = $request->query->get('user_id');
        $groupId = $request->query->get('group_id');

        $group = $this->groupService->addUser2Group($userId, $groupId);

        return new JsonResponse($group->toArray(), 200);
    }

    /**
     * @Route ("/task", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createTaskAction(Request $request): Response
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');

        $task = $this->taskService->createTask($name, $description);

        return new JsonResponse($task->toArray(), 200);
    }

    /**
     * @Route ("/lesson", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createLessonAction(Request $request): Response
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $courseId = $request->request->get('course_id');
        $task = $request->request->get('task');

        /**$name = '4342';
        $description = '4324';
        $courseId = 2;
         * */
        $task = [1,2,3];


        $lesson = $this->lessonService->createLesson($name, $description, $courseId, $task);

        return new JsonResponse($lesson->toArray(), 200);
    }

    /**
     * @Route ("/course", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createCourseAction(Request $request): Response
    {
        $name = $request->request->get('name');
        $course = $this->courseService->createCourse($name);

        return new JsonResponse($course->toArray(), 200);
    }

    /**
     * @Route ("/revision", methods={"POST"})
     * @return Response
     */
    public function createRevisionAction(): Response
    {
        $revision = $this->revisionService->next();

        return new JsonResponse($revision->toArray(), 200);
    }

    /**
     * @Route ("/group", methods={"POST"})
     * @return Response
     */
    public function createGroupAction(Request $request): Response
    {
        $courseId = $request->request->get('course_id');
        $userId = $request->request->get('user_id');
        $group = $this->groupService->createGroup($courseId, $userId);

        return new JsonResponse($group->toArray(), 200);
    }
}
