<?php

namespace App\Controller;

use App\Service\CourseService;
use App\Service\GroupService;
use App\Service\LessonService;
use App\Service\RevisionService;
use App\Service\TaskService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @var UserService
     */
    private UserService $userService;

    /**
     * @var CourseService
     */
    private CourseService $courseService;

    /**
     * @var GroupService
     */
    private GroupService $groupService;

    /**
     * @var LessonService
     */
    private LessonService $lessonService;

    /**
     * @var RevisionService
     */
    private RevisionService $revisionService;

    /**
     * @var TaskService
     */
    private TaskService $taskService;

    public function __construct(
        UserService $userService,
        CourseService $courseService,
        GroupService $groupService,
        LessonService $lessonService,
        RevisionService $revisionService,
        TaskService $taskService
    ) {
        $this->userService = $userService;
        $this->courseService = $courseService;
        $this->groupService = $groupService;
        $this->lessonService = $lessonService;
        $this->revisionService = $revisionService;
        $this->taskService = $taskService;
    }

    public function hello(): Response
    {
        //$user = $this->userService->createUser();

        //return $this->json($user->toArray());

        $revision = $this->revisionService::next();

        $task = $this->taskService->addTask();


        $lesson = $this->lessonService->addLesson($revision, $task);
        dump($lesson->toArray());
        return $this->json($task->toArray());
    }
}
